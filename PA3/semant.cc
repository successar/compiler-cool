

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include "semant.h"
#include "utilities.h"
#include <vector>


extern int semant_debug;
extern char *curr_filename;
ClassTable *classtable;

//////////////////////////////////////////////////////////////////////
//
// Symbols
//
// For convenience, a large number of symbols are predefined here.
// These symbols include the primitive type and method names, as well
// as fixed names used by the runtime system.
//
//////////////////////////////////////////////////////////////////////
static Symbol 
    arg,
    arg2,
    Bool,
    concat,
    cool_abort,
    copy,
    Int,
    in_int,
    in_string,
    IO,
    length,
    Main,
    main_meth,
    No_class,
    No_type,
    Object,
    out_int,
    out_string,
    prim_slot,
    self,
    SELF_TYPE,
    Str,
    str_field,
    substr,
    type_name,
    val;
//
// Initializing the predefined symbols.
//
static void initialize_constants(void)
{
    arg         = idtable.add_string("arg");
    arg2        = idtable.add_string("arg2");
    Bool        = idtable.add_string("Bool");
    concat      = idtable.add_string("concat");
    cool_abort  = idtable.add_string("abort");
    copy        = idtable.add_string("copy");
    Int         = idtable.add_string("Int");
    in_int      = idtable.add_string("in_int");
    in_string   = idtable.add_string("in_string");
    IO          = idtable.add_string("IO");
    length      = idtable.add_string("length");
    Main        = idtable.add_string("Main");
    main_meth   = idtable.add_string("main");
    //   _no_class is a symbol that can't be the name of any 
    //   user-defined class.
    No_class    = idtable.add_string("_no_class");
    No_type     = idtable.add_string("_no_type");
    Object      = idtable.add_string("Object");
    out_int     = idtable.add_string("out_int");
    out_string  = idtable.add_string("out_string");
    prim_slot   = idtable.add_string("_prim_slot");
    self        = idtable.add_string("self");
    SELF_TYPE   = idtable.add_string("SELF_TYPE");
    Str         = idtable.add_string("String");
    str_field   = idtable.add_string("_str_field");
    substr      = idtable.add_string("substr");
    type_name   = idtable.add_string("type_name");
    val         = idtable.add_string("_val");
}



ClassTable::ClassTable(Classes classes) : semant_errors(0) , error_stream(cerr) {
    inhrt = classes;
    env = new typeEnv();
    /* Fill this in */

}

void ClassTable::check() {

   Classes classes = inhrt;
   if(!find(Main)) semant_error() << "Class Main is not defined.\n";
   for(int i = classes->first(); classes->more(i); i = classes->next(i))
     {  

       Class_ req = classes->nth(i);

       if(req->get_name() == Int || req->get_name() == Str || req->get_name() == Bool || req->get_name() == SELF_TYPE || req->get_name() == Object || req->get_name() == IO ) 
	 { semant_error(req); return ; }      // basic classes not redefined

       if(!( find(req->get_parent()) || req->get_parent() == Object || req->get_parent() == IO ) )
	 { semant_error(req); return ; }       // valid parents

     }

   install_basic_classes();
   make_graph();
   attr_check();
   method_check();
   if(semant_errors) return;
   createTypeEnv();
   typeCheck();

}

void ClassTable::make_graph() {

  Classes classes = inhrt;
  std::vector<Symbol> cla;
  for(int i = classes->first(); classes->more(i); i = classes->next(i)){
    
    bool cycle = false;
    Class_ req = classes->nth(i);
    Class_ cl = classes->nth(i);

    if(i == 0) cla.push_back(req->get_name());
    else {
      for(int j = 0; j < cla.size(); j++) {
	if(req->get_name() == cla[j]) { semant_error(req); return; }
      }
      cla.push_back(req->get_name());
    }

    while(!cycle && !(cl->get_parent() == No_class) ) {
      
      bool same = (cl->get_parent() == req->get_name());
      if( same ) { semant_error(req); return ;}
      else cl = find_class(cl->get_parent());

    }   

  }

}

void ClassTable::attr_check() {

  Classes classes = inhrt;
  for(int i = classes->first(); classes->more(i); i = classes->next(i)) {
 
    Class_ req = classes->nth(i); 
    if(req->get_name() == Object || req->get_name() == IO || req->get_name() == Int || req->get_name() == Str || req->get_name() == Bool ) return;
 
    Class_ cl = find_class(req->get_parent());

    Features ref = req->get_features();
    Features clf = cl->get_features() ;

    for(int j = clf->first(); clf->more(j); j = clf->next(j)) {

      Feature f = clf->nth(j);
      if(f->get_type() == ATTR_ID) {

	for(int k = ref->first(); ref->more(k); k = ref->next(k)) {
	  Feature g = ref->nth(k);

          if(g->get_type() == ATTR_ID && g->get_name() == f->get_name()) {
    
            semant_error(req->get_filename(), g);
            return;

	  
	 }
        }
      }
    }
  }
}

void ClassTable::method_check() {
  
  Classes classes = inhrt;
  for(int i = classes->first(); classes->more(i); i = classes->next(i)) {
 
    Class_ req = classes->nth(i); 
    if(req->get_name() == Object || req->get_name() == IO || req->get_name() == Int || req->get_name() == Str || req->get_name() == Bool ) return; 
    Class_ cl = find_class(req->get_parent());

    Features ref = req->get_features();
    Features clf = cl->get_features() ;

    for(int j = clf->first(); clf->more(j); j = clf->next(j)) {

      Feature f = clf->nth(j);
      Formals ff = f->get_formals();
      if(f->get_type() == METHOD_ID) {

	for(int k = ref->first(); ref->more(k); k = ref->next(k)) {
	  Feature g = ref->nth(k);
          Formals gf = g->get_formals();

          if(g->get_type() == METHOD_ID && g->get_name() == f->get_name()) {
            bool same_t = (f->get_t() == g->get_t()) ;
            bool same_len = (ff->len() == gf->len()) ;
            bool same_f = true;
            if(same_len) {
              for(int l = gf->first(); gf->more(l); l = gf->next(l)) {
		if(gf->nth(l)->get_type() != ff->nth(l)->get_type()) 
		  same_f = false;
	      }
	    }
            
            if(!same_t || !same_len || !same_f ) 
              {semant_error(req->get_filename(), g);
	      return;}

	  
	 }
        }
      }
    }
  }
}


void ClassTable::install_basic_classes() {

    // The tree package uses these globals to annotate the classes built below.
   // curr_lineno  = 0;
    Symbol filename = stringtable.add_string("<basic class>");
    
    // The following demonstrates how to create dummy parse trees to
    // refer to basic Cool classes.  There's no need for method
    // bodies -- these are already built into the runtime system.
    
    // IMPORTANT: The results of the following expressions are
    // stored in local variables.  You will want to do something
    // with those variables at the end of this method to make this
    // code meaningful.

    // 
    // The Object class has no parent class. Its methods are
    //        abort() : Object    aborts the program
    //        type_name() : Str   returns a string representation of class name
    //        copy() : SELF_TYPE  returns a copy of the object
    //
    // There is no need for method bodies in the basic classes---these
    // are already built in to the runtime system.

    Class_ Object_class =
	class_(Object, 
	       No_class,
	       append_Features(
			       append_Features(
					       single_Features(method(cool_abort, nil_Formals(), Object, no_expr())),
					       single_Features(method(type_name, nil_Formals(), Str, no_expr()))),
			       single_Features(method(copy, nil_Formals(), SELF_TYPE, no_expr()))),
	       filename);

    // 
    // The IO class inherits from Object. Its methods are
    //        out_string(Str) : SELF_TYPE       writes a string to the output
    //        out_int(Int) : SELF_TYPE            "    an int    "  "     "
    //        in_string() : Str                 reads a string from the input
    //        in_int() : Int                      "   an int     "  "     "
    //
    Class_ IO_class = 
	class_(IO, 
	       Object,
	       append_Features(
			       append_Features(
					       append_Features(
							       single_Features(method(out_string, single_Formals(formal(arg, Str)),
										      SELF_TYPE, no_expr())),
							       single_Features(method(out_int, single_Formals(formal(arg, Int)),
										      SELF_TYPE, no_expr()))),
					       single_Features(method(in_string, nil_Formals(), Str, no_expr()))),
			       single_Features(method(in_int, nil_Formals(), Int, no_expr()))),
	       filename);  

    //
    // The Int class has no methods and only a single attribute, the
    // "val" for the integer. 
    //
    Class_ Int_class =
	class_(Int, 
	       Object,
	       single_Features(attr(val, prim_slot, no_expr())),
	       filename);

    //
    // Bool also has only the "val" slot.
    //
    Class_ Bool_class =
	class_(Bool, Object, single_Features(attr(val, prim_slot, no_expr())),filename);

    //
    // The class Str has a number of slots and operations:
    //       val                                  the length of the string
    //       str_field                            the string itself
    //       length() : Int                       returns length of the string
    //       concat(arg: Str) : Str               performs string concatenation
    //       substr(arg: Int, arg2: Int): Str     substring selection
    //       
    Class_ Str_class =
	class_(Str, 
	       Object,
	       append_Features(
			       append_Features(
					       append_Features(
							       append_Features(
									       single_Features(attr(val, Int, no_expr())),
									       single_Features(attr(str_field, prim_slot, no_expr()))),
							       single_Features(method(length, nil_Formals(), Int, no_expr()))),
					       single_Features(method(concat, 
								      single_Formals(formal(arg, Str)),
								      Str, 
								      no_expr()))),
			       single_Features(method(substr, 
						      append_Formals(single_Formals(formal(arg, Int)), 
								     single_Formals(formal(arg2, Int))),
						      Str, 
						      no_expr()))),
	       filename);


    inhrt = append_Classes(inhrt, single_Classes(Str_class));
    inhrt = append_Classes(inhrt, single_Classes(IO_class));
    inhrt = append_Classes(inhrt, single_Classes(Int_class));
    inhrt = append_Classes(inhrt, single_Classes(Bool_class));
    inhrt = append_Classes(inhrt, single_Classes(Object_class));

}

void ClassTable::createTypeEnv() {
  
  for(int i = inhrt->first(); inhrt->more(i); i = inhrt->next(i)) {
    Class_ req = inhrt->nth(i);    
    Features f = req->get_features();

    for(int j = f->first(); f->more(j); j = f->next(j)) {
      Feature ff = f->nth(j);
      if(ff->get_type() == METHOD_ID) {
        env->MT_insert(ff->get_name(), req->get_name(), ff->get_formals(), ff->get_t());
        
      }
    }
  }
}

Symbol ClassTable::lub(Symbol t1, Symbol t2) {
  
  if(t1 == SELF_TYPE && t2 == SELF_TYPE) return SELF_TYPE;
  if(t1 == SELF_TYPE) return lub(env->get_currcl()->get_name(), t2);
  if(t2 == SELF_TYPE) return lub(env->get_currcl()->get_name(), t1);
  Class_ c1 = find_class(t1);
  Class_ c2 = find_class(t2);
  std::vector<Symbol> a1, a2, ab, as;

  while(c1->get_name() != Object) { 
    a1.push_back(c1->get_name());
    c1 = find_class(c1->get_parent());
  }
  a1.push_back(Object);

  while(c2->get_name() != Object) { 
    a2.push_back(c2->get_name());
    c2 = find_class(c2->get_parent());
  }
  a2.push_back(Object);
 
  ab = (a1.size() >= a2.size()) ? a1 : a2;
  as = (a1.size() < a2.size()) ? a1 : a2;

  for(int i = 0; i < as.size(); i++) {
    for(int j = 0; j < ab.size(); j++) {
      if(as[i] == ab[j]) return as[i];
    }
  }
  
  return Object;
}

bool ClassTable::typeComp(Symbol t1, Symbol t2) {
  if(t1 == No_type) return true;
  if(t1 == SELF_TYPE && t2 == SELF_TYPE) return true;
  if(t2 == SELF_TYPE) return false;
  if(t1 == SELF_TYPE) {
    return typeComp(env->get_currcl()->get_name(), t2);
  }
  Class_ c1 = find_class(t1);

  while(c1->get_name() != No_class) {
    if(c1->get_name() == t2) return true;
    else if( c1->get_name() == Object ) { 
      if(t2 == Object) return true;
      else return false;
    }
    else c1 = find_class(c1->get_parent());
  }

  return false;
}

void ClassTable::typeCheck() {
 
  for(int i = inhrt->first(); inhrt->more(i); i = inhrt->next(i)) {
    Class_ req = inhrt->nth(i);
    Features f = req->get_features();
    
    env->set_currcl(req);

    env->OT_enter();
    for(int j = f->first(); f->more(j); j = f->next(j)) {
      if(f->nth(j)->get_type() == ATTR_ID) {
        if(f->nth(j)->get_name() == self) { semant_error(req); }
        else env->OT_insert(f->nth(j)->get_name(), f->nth(j)->get_t());
      }
   }
   
    Class_ cl = req;
   
   
   while(cl->get_name() != Object) {
   cl = find_class(cl->get_parent());
   Features g = cl->get_features();
   for(int j = g->first(); g->more(j); j = g->next(j)) {
      if(g->nth(j)->get_type() == ATTR_ID) {
        if(g->nth(j)->get_name() == self) {  }
        else env->OT_insert(g->nth(j)->get_name(), g->nth(j)->get_t()); 
      }
     }
   }

   for(int j = f->first(); f->more(j); j = f->next(j)) {
      if(f->nth(j)->get_type() == ATTR_ID) {
        f->nth(j)->get_expr()->typecheck(env); 
      }
   }

   for(int j = f->first(); f->more(j); j = f->next(j)) {
     if(f->nth(j)->get_type() == METHOD_ID){
       env->OT_enter();
       env->OT_insert(self, SELF_TYPE);
       Formals ff = f->nth(j)->get_formals();       
       for(int k = ff->first(); ff->more(k); k = ff->next(k)) {
         Formal form = ff->nth(k);
	 if(form->get_name() == self || form->get_type() == SELF_TYPE|| env->OT_probe(form->get_name())) semant_error(req);
       
         else env->OT_insert(form->get_name(), form->get_type());
       }
      
       Expression e = f->nth(j)->get_expr();
       Symbol t = e->typecheck(env);
       Symbol r_t = f->nth(j)->get_t();
       if( r_t != SELF_TYPE && !find(r_t)) semant_error(req);
       else if(!typeComp(t, r_t)) semant_error(req);
       
       env->OT_exit();
      }
    }

    env->OT_exit();
  }
}

Symbol no_expr_class::typecheck(typeEnv *env) {
  set_type(No_type);
  return No_type;
}

Symbol object_class::typecheck(typeEnv *env) {
  Symbol t;
  if(name == self) t = SELF_TYPE;
  else if(env->OT_lookup(name)) {
  t = env->OT_value(name);
  }
  else {
  t = Object;
  classtable->semant_error() << "filem.test:1:object" << name << " " << env->get_currcl()->get_name() << "\n";
  }
  set_type(t);
  return t; 
}

Symbol int_const_class::typecheck(typeEnv *env) {
  set_type(Int);
  return Int; } 

Symbol bool_const_class::typecheck(typeEnv *env) {
  set_type(Bool);
  return Bool; }    

Symbol string_const_class::typecheck(typeEnv *env) {
  set_type(Str);
  return Str; }

Symbol new__class::typecheck(typeEnv *env) {

  Symbol type = Object;
  if(type_name == SELF_TYPE) type = SELF_TYPE;
  else if(!classtable->find(type_name)) classtable->semant_error() << "filem.test:1:new\n";
  else type = type_name;

  set_type(type);
  return type; 

}    
    
Symbol isvoid_class::typecheck(typeEnv *env) {
  e1->typecheck(env);
  set_type(Bool);
  return Bool; }  

Symbol lt_class::typecheck(typeEnv *env) {
  Symbol type;
  bool error = false;
  if(e1->typecheck(env) == Int && e2->typecheck(env) == Int) type = Bool;
  else { type = Object; error = true; }
  if(error) classtable->semant_error() << "filem.test:1:lt\n";
  set_type(type);
  return type; }    

Symbol leq_class::typecheck(typeEnv *env) {
  Symbol type; bool error = false;
  Symbol t1  = e1->typecheck(env), t2 = e2->typecheck(env);
  if(t1  == Int &&  t2 == Int) type = Bool;
  else { type = Object; error = true; }
  if(error) classtable->semant_error() << "filem.test:1:lq\n";
  set_type(type);
  return type; }    
  
Symbol comp_class::typecheck(typeEnv *env) {
  Symbol type;
  if(e1->typecheck(env) == Bool) type = Bool;
  else { type = Object; classtable->semant_error() << "filem.test:1:comp\n"; }
  set_type(type);
  return type;
}

Symbol neg_class::typecheck(typeEnv *env) {
  Symbol type;
  if(e1->typecheck(env) == Int) type = Int;
  else { type = Object; classtable->semant_error() << "filem.test:1:neg\n"; }
  set_type(type);
  return type;
}

Symbol divide_class::typecheck(typeEnv *env) {
  Symbol type;
  Symbol t1 = e1->typecheck(env), t2 = e2->typecheck(env);
  if(t1 == Int && t2 == Int) type = Int;
  else { type = Object; classtable->semant_error() << "filem.test:1:div\n"; }
  set_type(type);
  return type;
}

Symbol mul_class::typecheck(typeEnv *env) {
  Symbol type;
  Symbol t1 = e1->typecheck(env), t2 = e2->typecheck(env);
  if(t1 == Int && t2 == Int) type = Int;
  else { type = Object; classtable->semant_error() << "filem.test:1:mul\n"; }
  set_type(type);
  return type;
}

Symbol sub_class::typecheck(typeEnv *env) {
  Symbol type;
  Symbol t1 = e1->typecheck(env), t2 = e2->typecheck(env);
  if(t1 == Int && t2 == Int) type = Int;
  else { type = Object; classtable->semant_error() << "filem.test:1:sub\n"; }
  set_type(type);
  return type;
}

Symbol plus_class::typecheck(typeEnv *env) {
  Symbol type;
  Symbol t1 = e1->typecheck(env), t2 = e2->typecheck(env);
  if(t1 == Int && t2 == Int) type = Int;
  else { type = Object; classtable->semant_error() << "filem.test:1:plus\n"; }
  set_type(type);
  return type;
}

Symbol block_class::typecheck(typeEnv *env) {
  for(int i = body->first(); body->more(i); i = body->next(i)){
    Symbol type = body->nth(i)->typecheck(env);
    if(!body->more(body->next(i))) {
      set_type(type);
      return type;
    }
  }
}

Symbol loop_class::typecheck(typeEnv *env) {
  Symbol type;
  body->typecheck(env);
  if(pred->typecheck(env) == Bool) type = Object;
  else { type = Object; classtable->semant_error() << "filem.test:1:loop\n"; }
  set_type(type);
  return type; }

Symbol cond_class::typecheck(typeEnv *env) {
  Symbol type;
  Symbol then_t = then_exp->typecheck(env), else_t = else_exp->typecheck(env);
  if(pred->typecheck(env) == Bool) {
    type = classtable->lub(then_t, else_t);
  }
  else { type = Object; classtable->semant_error() << "filem.test:1:cond\n"; }
  set_type(type);
  return type;
}
  
Symbol let_class::typecheck(typeEnv *env) {
  bool error = false;

  if(identifier == self) error = true;
  Symbol init_type = init->typecheck(env);
  
  if( init_type != No_type ) {
    if(!classtable->typeComp(init_type, type_decl)) { error = true; }
  }

  env->OT_enter();
  env->OT_insert(identifier, type_decl);
  Symbol type = body->typecheck(env);
  env->OT_exit();

  if(error) classtable->semant_error() << "filem.test:1:let\n";
  set_type(type);
  return type; }

Symbol typcase_class::typecheck(typeEnv *env) {

  expr->typecheck(env);  bool error = false;
  Symbol c_lub;
  Arg_types t;

  for(int i = cases->first(); cases->more(i); i = cases->next(i) ){
    Case c = cases->nth(i);
    if(i == cases->first()) { t.push_back(c->get_t()); }
    else {
      for(int j = 0; j < t.size(); j++) {
	if(c->get_t() == t[j]) { 
            error = true; 
          }
      }
      t.push_back(c->get_t());
    }
 
    env->OT_enter();
    env->OT_insert(c->get_name(), c->get_t());
    Symbol t =  c->get_expr()->typecheck(env);
    env->OT_exit();
    if(i == cases->first()) c_lub = t;
    else c_lub = classtable->lub(c_lub, t);

  }
  if( error ) classtable->semant_error() << "filem.test:1:\n";
  set_type(c_lub);
  return c_lub;
}

Symbol eq_class::typecheck(typeEnv *env) {
  
  Symbol type = Bool;
  Symbol t1 = e1->typecheck(env);
  Symbol t2 = e2->typecheck(env);
  if(t1 == Int || t1 == Str || t1 == Bool){
    if(t2 != t1) { 
      type = Object; 
      classtable->semant_error() << "filem.test:" << "1:eq\n" ;
      }
  
    else if(t2 == Int || t2 == Str || t2 == Bool){
    if(t2 != t1) { 
       type = Object;
       classtable->semant_error() << "filem.test:" << "1:\n" ;
       
    }
   }
   }
  
  set_type(type);
  return type; 

} 

Symbol assign_class::typecheck(typeEnv *env) {
  Symbol type = Object;
  Symbol expr_type = expr->typecheck(env);
  bool error = false;
  if( name == self ) error = true;
  if(env->OT_lookup(name))
    { Symbol id_type = env->OT_value(name);
      if( classtable->typeComp(expr_type, id_type) ) 
        type = expr_type;
      else { error = true; }
    }
  else { error = true; }

  if( error ) classtable->semant_error() << "filem.test:1:assign\n";
  set_type(type);  
  return type;
}

Symbol dispatch_class::typecheck(typeEnv *env) {
  
  bool error = false;
  Symbol call_type = expr->typecheck(env), call_type_dash; 
  
  Symbol type = Object;
  if(call_type == SELF_TYPE) call_type_dash = env->get_currcl()->get_name();
  else call_type_dash = call_type;
  
  Class_ cl = classtable->find_class(call_type_dash);
  while (cl->get_name() != No_class) {

    if(env->MT_lookup(name, cl->get_name())) {

      values *v = env->MT_value(name, cl->get_name());
      Arg_types *t0 = v->types;
  
    for(int i = actual->first(); actual->more(i); i = actual->next(i)){
      Expression e = actual->nth(i);
      Symbol t = e->typecheck(env);
      if(!classtable->typeComp(t, (*t0)[i])) error = true;
    }
  
    if(v->return_t == SELF_TYPE) type = call_type;
    else type = v->return_t;
    
    break;

   }

    if(cl->get_name() == Object) { error = true; break; }
    else cl = classtable->find_class(cl->get_parent());
  }

  if(cl->get_name() == No_class) error = true;

  if(error) classtable->semant_error() << "filem.test:1:dispatch\n";
  set_type(type);
  return type; 

 }

Symbol static_dispatch_class::typecheck(typeEnv *env) {

  bool error = false;
  Symbol call_type = expr->typecheck(env), type = Object;
  if(!classtable->typeComp(call_type, type_name)) error = true;

  Class_ cl = classtable->find_class(type_name);

  while(cl->get_name() != No_class) {
    
    if(env->MT_lookup(name, cl->get_name())) {
  
      values *v = env->MT_value(name, cl->get_name() );
      Arg_types *t0 = v->types;

      for(int i = actual->first(); actual->more(i); i = actual->next(i)){
	Expression e = actual->nth(i);
	Symbol t = e->typecheck(env);
        if(!classtable->typeComp(t, (*t0)[i])) error = true;
      }

      if(v->return_t == SELF_TYPE) type = call_type;
      else type = v->return_t;
      break;
    }
    
    cl = classtable->find_class(cl->get_parent());

  }
  
  if(error) classtable->semant_error() << "filem.test:1:static\n";
  set_type(type);
  return type; 

 }

  
  
    
////////////////////////////////////////////////////////////////////
//
// semant_error is an overloaded function for reporting errors
// during semantic analysis.  There are three versions:
//
//    ostream& ClassTable::semant_error()                
//
//    ostream& ClassTable::semant_error(Class_ c)
//       print line number and filename for `c'
//
//    ostream& ClassTable::semant_error(Symbol filename, tree_node *t)  
//       print a line number and filename
//
///////////////////////////////////////////////////////////////////

ostream& ClassTable::semant_error(Class_ c)
{                                                             
    return semant_error(c->get_filename(),c);
}    

ostream& ClassTable::semant_error(Symbol filename, tree_node *t)
{
    error_stream << filename << ":" << t->get_line_number() << ":\n";
    return semant_error();
}

ostream& ClassTable::semant_error()                  
{                                                 
    semant_errors++;                            
    return error_stream;
} 



/*   This is the entry point to the semantic checker.

     Your checker should do the following two things:

     1) Check that the program is semantically correct
     2) Decorate the abstract syntax tree with type information
        by setting the `type' field in each Expression node.
        (see `tree.h')

     You are free to first do 1), make sure you catch all semantic
     errors. Part 2) can be done in a second stage, when you want
     to build mycoolc.
 */
void program_class::semant()
{
    initialize_constants();
    /* ClassTable constructor may do some semantic analysis */
    classtable = new ClassTable(classes);
    classtable->check();

    /* some semantic analysis code may go here */
    if (classtable->errors()) {
	cerr << "Compilation halted due to static semantic errors." << endl;
	exit(1);
    }
}


