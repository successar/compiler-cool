#ifndef SEMANT_H_
#define SEMANT_H_

#include <assert.h>
#include <iostream>  
#include "cool-tree.h"
#include "stringtab.h"
#include "symtab.h"
#include "list.h"
#include "vector"
//using namespace std;

#define TRUE 1
#define FALSE 0

class ClassTable;
typedef ClassTable *ClassTableP;

// This is a structure that may be used to contain the semantic
// information such as the inheritance graph.  You may use it or not as
// you like: it is only here to provide a container for the supplied
// methods.

class ClassTable {
private:
  Classes inhrt;
  typeEnv *env;
  void attr_check();
  void method_check();
  void make_graph();
    
  int semant_errors;
  void install_basic_classes();

  void createTypeEnv();
  void typeCheck();
  
  ostream& error_stream;

public:
  ClassTable(Classes);
  int errors() { return semant_errors; }
  void check();
  bool typeComp(Symbol t1, Symbol t2);
  Symbol lub(Symbol t1, Symbol t2);
  ostream& semant_error();
  ostream& semant_error(Class_ c);
  ostream& semant_error(Symbol filename, tree_node *t);
  bool find(Symbol p) {
    bool ret = false;
    for(int i = inhrt->first(); inhrt->more(i); i = inhrt->next(i))
      {   if(inhrt->nth(i)->get_name() == p) ret = true; }
    
    return ret;
  }

  Class_ find_class(Symbol p) {

   for(int i = inhrt->first(); inhrt->more(i); i = inhrt->next(i))
      {   if(inhrt->nth(i)->get_name() == p) 
          return inhrt->nth(i);
      }
  }
};

  
#endif

