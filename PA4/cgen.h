#include <assert.h>
#include <stdio.h>
#include "emit.h"
#include "cool-tree.h"
#include "symtab.h"

enum Basicness     {Basic, NotBasic};
#define TRUE 1
#define FALSE 0
#define ATTR 1
#define FORMAL 2
#define LOCAL 3

class CgenClassTable;
typedef CgenClassTable *CgenClassTableP;

class CgenNode;
typedef CgenNode *CgenNodeP;

struct Obj_node {

  Symbol type;
  int nature;
  int slot;

};

struct Method_node {
  Symbol name;
  CgenNodeP nd;
  bool acc;

};

class Method_lookuptbl : public SymbolTable<CgenNodeP, List<Method_node> > {

 public :
  void enter_value(CgenNodeP nd, List<Method_node>* tbl) {
    addid(nd, tbl);
  }

  List<Method_node>* look(CgenNodeP nd) {
    return lookup(nd);
  }
};

class Obj_lookuptbl : public SymbolTable<Symbol, Obj_node> {

 public:
  void enter_value(Symbol name, Symbol t, int n, int s) {
    Obj_node* ptr = new Obj_node();
    ptr->type = t; ptr->nature = n; ptr->slot = s;
    addid(name, ptr);
  }

  Obj_node* get_value(Symbol name) {
    return lookup(name);
  }
    

};

class CgenNode : public class__class {
 private: 
  CgenNodeP parentnd;                        // Parent of class
  List<CgenNode> *children;                  // Children of class
  Basicness basic_status; 
  int class_tag; 
  // `Basic' if class is basic
  // `NotBasic' otherwise

 public:
  CgenNode(Class_ c,
	   Basicness bstatus,
	   CgenClassTableP class_table);
  Symbol get_name() { return name; }
  Features get_features() { return features; }
  void add_child(CgenNodeP child);
  List<CgenNode> *get_children() { return children; }
  void set_parentnd(CgenNodeP p);
  CgenNodeP get_parentnd() { return parentnd; }
  int basic() { return (basic_status == Basic); }
  int get_tag() { return class_tag; }
  void set_tag(int t) { class_tag = t; }
  int lchild;
};


class CgenClassTable : public SymbolTable<Symbol,CgenNode> {
 public:
  List<CgenNode> *nds;
  ostream& str;
  int stringclasstag;
  int intclasstag;
  int boolclasstag;
  int clt ;

  // The following methods emit code for
  // constants and global declarations.

  void code_global_data();
  void code_global_text();
  void code_bools(int);
  void code_select_gc();
  void code_constants();

  int set_tag(CgenNodeP nd, int index) {
    nd->set_tag(index);
    if(nd->get_name() ==idtable.lookup_string("String")) stringclasstag = index;
    else if(nd->get_name() == idtable.lookup_string("Int")) intclasstag = index;
    else if(nd->get_name() ==idtable.lookup_string("Bool")) boolclasstag =index;

    for(List<CgenNode> *l = nd->get_children(); l; l = l->tl()) {
      index++;
      index = set_tag(l->hd(), index);
    }
    nd->lchild = index ;
    return index;
  }

  void set_tags() {
    set_tag(root(), 0);
  }

  // The following creates an inheritance graph from
  // a list of classes.  The graph is implemented as
  // a tree of `CgenNode', and class names are placed
  // in the base class symbol table.

  void install_basic_classes();
  void install_class(CgenNodeP nd);
  void install_classes(Classes cs);
  void build_inheritance_tree();
  void set_relations(CgenNodeP nd);

  void code_Tabs() {
    str << CLASSNAMETAB << ":" << endl;
    classTab_recurse(root());
    str << CLASSOBJTAB << ":" << endl;
    objTab_recurse(root());
  }

  void classTab_recurse(CgenNodeP nd) {
    str << WORD << STRCONST_PREFIX << stringtable.lookup_string(nd->get_name()->get_string())->index << endl;
    for(List<CgenNode> *l = nd->get_children(); l; l = l->tl()) {   
      classTab_recurse(l->hd());
    }
  }
    
    
  void objTab_recurse(CgenNodeP nd) {
    str << WORD;
    str << nd->get_name() << PROTOBJ_SUFFIX;
    str << endl;
    str << WORD;
    str << nd->get_name() << CLASSINIT_SUFFIX;
    str << endl;
    for(List<CgenNode> *l = nd->get_children(); l; l = l->tl()) {   
      objTab_recurse(l->hd());
    }
  }

  void code_dispTab(List<CgenNode> *j) {
    for(List<CgenNode> *l = j; l; l = l->tl()) {
      str << l->hd()->get_name() << DISPTAB_SUFFIX << ":" << endl;
      List<Method_node>* m = create_ml(l->hd());
      for(; m->hd() != NULL ; m = m->tl()) {
	if(!m->hd()->acc) {
	  str << WORD << m->hd()->nd->get_name() << "." << m->hd()->name << endl;
	}
      }
    }
  }

  void code_protoObj(List<CgenNode> *j) {

    for(List<CgenNode> *l = j; l; l = l->tl()) {
      str << l->hd()->get_name() << PROTOBJ_SUFFIX << ":" << endl;
      str << WORD << l->hd()->get_tag() << endl;
      str << WORD << count_attr(l->hd()) << endl;
      str << WORD << l->hd()->get_name() << DISPTAB_SUFFIX << endl;
      print_attr(l->hd());
      str << WORD << -1 << endl;
    }
  }

   
   
  int count_attr(CgenNodeP nd) {
    int x = 0;

    if(nd->get_name() == idtable.lookup_string("Object")) 
      return DEFAULT_OBJFIELDS;

    if(nd->get_parentnd()) {
      x = count_attr(nd->get_parentnd());
    }
     
    Features f = nd->get_features();
     
    for(int i = f->first(); f->more(i); i = f->next(i)) {
      Feature ff = f->nth(i);
      if(ff->get_t() == ATTR_ID) { x++; }
    }

    return x;
  }
           
  void print_attr(CgenNodeP nd) {
    if(nd->get_parentnd()) {
      print_attr(nd->get_parentnd());
    }
    Features f = nd->get_features();
    for(int i = f->first(); f->more(i); i = f->next(i)) {
      Feature ff = f->nth(i);
      if(ff->get_t() == ATTR_ID) {
	str << WORD ;
	if(ff->get_type() == idtable.lookup_string("String"))
	  str << STRCONST_PREFIX << stringtable.lookup_string("")->index << endl;
	else if (ff->get_type() == idtable.lookup_string("Int")) 
	  str << INTCONST_PREFIX << inttable.lookup_string("0")->index << endl;
	else if (ff->get_type() == idtable.lookup_string("Bool"))
	  str << BOOLCONST_PREFIX << 0 << endl;
	else str << 0 << endl;
      }
    }
     
  }

  void code_inits(CgenNodeP nd);
  void code_methods(CgenNodeP nd);
  int code_attrs(CgenNodeP nd);
   
  void populate(); 

  List<Method_node> *create_ml(CgenNodeP nd) {
     
    List<Method_node>* m = new List<Method_node>(NULL);
    CgenNodeP np = nd;
    while(true){

      Features f = np->get_features();
      int len = f->len();
      for(int i = f->first(); f->more(i); i = f->next(i)) {

	Feature ff = f->nth(len - i - 1);
	if(ff->get_t() == METHOD_ID){

	  Method_node* ml = lookup(ff, m);
	  if(ml == NULL) {
	    Method_node* ml = new Method_node ;
	    ml->name = ff->get_name();
	    ml->nd = np; ml->acc = false;
	    m = new List<Method_node> (ml, m);
	  }
	  else {
	    ml->acc = true;
	    Method_node* mn = new Method_node;
	    mn->name = ff->get_name();
	    mn->nd = ml->nd;
	    mn->acc = false;
            m = new List<Method_node> (mn, m);
	  }
	}
      }
   
      if(np->get_parentnd()) np = np->get_parentnd();
      else break;
    }
    return m;
  }
  
  Method_node* lookup(Feature f, List<Method_node> *j) {
    for(List<Method_node> *l = j ; l->hd() != NULL ; l = l->tl()) {
      Method_node* ml = l->hd();
      if(ml->name == f->get_name() && ml->acc == false) return ml;
    }
    return NULL;
  }         
   
 public:
  CgenClassTable(Classes, ostream& str);
  void code();
  CgenNodeP root();

  int lookup_function(Symbol cl_name, Symbol f_name);
  CgenNodeP lookup_class(Symbol name) {
    for(List<CgenNode> *l = nds; l; l = l->tl()) {
      if(l->hd()->get_name() == name) return l->hd();
    }
    return NULL;
  }

  int max_case(Cases cases, int lmax) {
    int max = -1;
    int j = -1;
    for(int i = cases->first(); cases->more(i); i = cases->next(i)) {
      int m = lookup_class(cases->nth(i)->get_type_decl())->get_tag();
      if( m > max && m < lmax ) { j = i; max = m; }
      else continue;
    }
    return j;
  }


};



class BoolConst 
{
 private: 
  int val;
 public:
  BoolConst(int);
  void code_def(ostream&, int boolclasstag);
  void code_ref(ostream&) const;
};

