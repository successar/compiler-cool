class Main inherits IO {
  
  a(x : Int) : Int { x * x };
  b(x : Int, y: Int) : Int { y * x };
  
  main() : Object { 
    out_int(b(3, 2))
  };

  };