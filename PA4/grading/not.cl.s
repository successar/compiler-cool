# start of generated code
	.data
	.align	2
	.globl	class_nameTab
	.globl	Main_protObj
	.globl	Int_protObj
	.globl	String_protObj
	.globl	bool_const0
	.globl	bool_const1
	.globl	_int_tag
	.globl	_bool_tag
	.globl	_string_tag
_int_tag:
	.word	2
_bool_tag:
	.word	3
_string_tag:
	.word	4
	.globl	_MemMgr_INITIALIZER
_MemMgr_INITIALIZER:
	.word	_NoGC_Init
	.globl	_MemMgr_COLLECTOR
_MemMgr_COLLECTOR:
	.word	_NoGC_Collect
	.globl	_MemMgr_TEST
_MemMgr_TEST:
	.word	0
	.word	-1
str_const11:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const0
	.byte	0	
	.align	2
	.word	-1
str_const10:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Main"
	.byte	0	
	.align	2
	.word	-1
str_const9:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"String"
	.byte	0	
	.align	2
	.word	-1
str_const8:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Bool"
	.byte	0	
	.align	2
	.word	-1
str_const7:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const5
	.ascii	"Int"
	.byte	0	
	.align	2
	.word	-1
str_const6:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const6
	.ascii	"IO"
	.byte	0	
	.align	2
	.word	-1
str_const5:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"Object"
	.byte	0	
	.align	2
	.word	-1
str_const4:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const7
	.ascii	"_prim_slot"
	.byte	0	
	.align	2
	.word	-1
str_const3:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"SELF_TYPE"
	.byte	0	
	.align	2
	.word	-1
str_const2:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"_no_class"
	.byte	0	
	.align	2
	.word	-1
str_const1:
	.word	4
	.word	8
	.word	String_dispTab
	.word	int_const9
	.ascii	"<basic class>"
	.byte	0	
	.align	2
	.word	-1
str_const0:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const10
	.ascii	"./not.cl"
	.byte	0	
	.align	2
	.word	-1
int_const10:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	8
	.word	-1
int_const9:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	13
	.word	-1
int_const8:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	9
	.word	-1
int_const7:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	10
	.word	-1
int_const6:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	2
	.word	-1
int_const5:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	3
	.word	-1
int_const4:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	6
	.word	-1
int_const3:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	4
	.word	-1
int_const2:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	1
	.word	-1
int_const1:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	100
	.word	-1
int_const0:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
bool_const0:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
bool_const1:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	1
class_nameTab:
	.word	str_const5
	.word	str_const6
	.word	str_const7
	.word	str_const8
	.word	str_const9
	.word	str_const10
class_objTab:
	.word	Object_protObj
	.word	Object_init
	.word	IO_protObj
	.word	IO_init
	.word	Int_protObj
	.word	Int_init
	.word	Bool_protObj
	.word	Bool_init
	.word	String_protObj
	.word	String_init
	.word	Main_protObj
	.word	Main_init
Main_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	Main.main
String_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	String.length
	.word	String.concat
	.word	String.substr
Bool_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
Int_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
IO_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	IO.out_string
	.word	IO.out_int
	.word	IO.in_string
	.word	IO.in_int
Object_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
Main_protObj:
	.word	5
	.word	3
	.word	Main_dispTab
	.word	-1
String_protObj:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const0
	.word	0
	.word	-1
Bool_protObj:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
Int_protObj:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
IO_protObj:
	.word	1
	.word	3
	.word	IO_dispTab
	.word	-1
Object_protObj:
	.word	0
	.word	3
	.word	Object_dispTab
	.word	-1
	.globl	heap_start
heap_start:
	.word	0
	.text
	.globl	Main_init
	.globl	Int_init
	.globl	String_init
	.globl	Bool_init
	.globl	Main.main
Main_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Main.main:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	la	$a0 int_const0
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	la	$a0 bool_const0
	sw	$a0 0($sp)
	addiu	$sp $sp -4
while0:
	lw	$a0 -4($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	la	$a0 int_const1
	lw	$t1 4($sp)
	addiu	$sp $sp 4
	lw	$a0 12($a0)
	lw	$t1 12($t1)
	slt	$t1 $t1 $a0
	beqz	$t1 not_lt0
	la	$a0 bool_const1
	b	 end_lt0
not_lt0:
	la	$a0 bool_const0
end_lt0:
	lw	$a0 12($a0)
	beqz	$a0 pool0
	lw	$a0 -8($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	lw	$a0 -8($fp)
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg0
	la	$a0 bool_const0
neg0:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg1
	la	$a0 bool_const0
neg1:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg2
	la	$a0 bool_const0
neg2:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg3
	la	$a0 bool_const0
neg3:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg4
	la	$a0 bool_const0
neg4:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg5
	la	$a0 bool_const0
neg5:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg6
	la	$a0 bool_const0
neg6:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg7
	la	$a0 bool_const0
neg7:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg8
	la	$a0 bool_const0
neg8:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg9
	la	$a0 bool_const0
neg9:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg10
	la	$a0 bool_const0
neg10:
	lw	$t1 4($sp)
	addiu	$sp $sp 4
	lw	$a0 12($a0)
	lw	$t1 12($t1)
	seq	$t1 $a0 $t1
	beqz	$t1 not_eq1
	la	$a0 bool_const1
	b	 end_eq1
not_eq1:
	la	$a0 bool_const0
end_eq1:
	lw	$a0 -8($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	lw	$a0 -8($fp)
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg11
	la	$a0 bool_const0
neg11:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg12
	la	$a0 bool_const0
neg12:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg13
	la	$a0 bool_const0
neg13:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg14
	la	$a0 bool_const0
neg14:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg15
	la	$a0 bool_const0
neg15:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg16
	la	$a0 bool_const0
neg16:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg17
	la	$a0 bool_const0
neg17:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg18
	la	$a0 bool_const0
neg18:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg19
	la	$a0 bool_const0
neg19:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg20
	la	$a0 bool_const0
neg20:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg21
	la	$a0 bool_const0
neg21:
	lw	$t1 4($sp)
	addiu	$sp $sp 4
	lw	$a0 12($a0)
	lw	$t1 12($t1)
	seq	$t1 $a0 $t1
	beqz	$t1 not_eq2
	la	$a0 bool_const1
	b	 end_eq2
not_eq2:
	la	$a0 bool_const0
end_eq2:
	lw	$a0 -8($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	lw	$a0 -8($fp)
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg22
	la	$a0 bool_const0
neg22:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg23
	la	$a0 bool_const0
neg23:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg24
	la	$a0 bool_const0
neg24:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg25
	la	$a0 bool_const0
neg25:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg26
	la	$a0 bool_const0
neg26:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg27
	la	$a0 bool_const0
neg27:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg28
	la	$a0 bool_const0
neg28:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg29
	la	$a0 bool_const0
neg29:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg30
	la	$a0 bool_const0
neg30:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg31
	la	$a0 bool_const0
neg31:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg32
	la	$a0 bool_const0
neg32:
	lw	$t1 4($sp)
	addiu	$sp $sp 4
	lw	$a0 12($a0)
	lw	$t1 12($t1)
	seq	$t1 $a0 $t1
	beqz	$t1 not_eq3
	la	$a0 bool_const1
	b	 end_eq3
not_eq3:
	la	$a0 bool_const0
end_eq3:
	lw	$a0 -8($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	lw	$a0 -8($fp)
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg33
	la	$a0 bool_const0
neg33:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg34
	la	$a0 bool_const0
neg34:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg35
	la	$a0 bool_const0
neg35:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg36
	la	$a0 bool_const0
neg36:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg37
	la	$a0 bool_const0
neg37:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg38
	la	$a0 bool_const0
neg38:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg39
	la	$a0 bool_const0
neg39:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg40
	la	$a0 bool_const0
neg40:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg41
	la	$a0 bool_const0
neg41:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg42
	la	$a0 bool_const0
neg42:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg43
	la	$a0 bool_const0
neg43:
	lw	$t1 4($sp)
	addiu	$sp $sp 4
	lw	$a0 12($a0)
	lw	$t1 12($t1)
	seq	$t1 $a0 $t1
	beqz	$t1 not_eq4
	la	$a0 bool_const1
	b	 end_eq4
not_eq4:
	la	$a0 bool_const0
end_eq4:
	lw	$a0 -8($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	lw	$a0 -8($fp)
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg44
	la	$a0 bool_const0
neg44:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg45
	la	$a0 bool_const0
neg45:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg46
	la	$a0 bool_const0
neg46:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg47
	la	$a0 bool_const0
neg47:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg48
	la	$a0 bool_const0
neg48:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg49
	la	$a0 bool_const0
neg49:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg50
	la	$a0 bool_const0
neg50:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg51
	la	$a0 bool_const0
neg51:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg52
	la	$a0 bool_const0
neg52:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg53
	la	$a0 bool_const0
neg53:
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 neg54
	la	$a0 bool_const0
neg54:
	lw	$t1 4($sp)
	addiu	$sp $sp 4
	lw	$a0 12($a0)
	lw	$t1 12($t1)
	seq	$t1 $a0 $t1
	beqz	$t1 not_eq5
	la	$a0 bool_const1
	b	 end_eq5
not_eq5:
	la	$a0 bool_const0
end_eq5:
	lw	$a0 -4($fp)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	la	$a0 int_const2
	lw	$t1 4($sp)
	lw	$t1 12($t1)
	lw	$t2 12($a0)
	add	$t2 $t2 $t1
	jal	Object.copy
	sw	$t2 12($a0)
	addiu	$sp $sp 4
	sw	$a0 -4($fp)
	b	 while0
pool0:
	lw	$a0 -8($fp)
	lw	$t1 12($a0)
	beqz	$t1 false6
true6:
	move	$a0 $s0
	bne	$a0 $zero label55
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label55:
	lw	$t1 8($a0)
	lw	$t1 0($t1)
	jalr		$t1
	b	 end_if6
false6:
	la	$a0 int_const0
end_if6:
	addiu	$sp $sp 4
	addiu	$sp $sp 4
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
String_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Bool_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Int_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
IO_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Object_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	

# end of generated code
