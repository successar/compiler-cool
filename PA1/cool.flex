/*
 *  The scanner definition for COOL.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */

%x COMMENT0 STRING COMMENT1 ERR_STRING
int nesting = 0;

%{
#include <cool-parse.h>
#include <stringtab.h>
#include <utilities.h>

/* The compiler assumes these identifiers. */
#define yylval cool_yylval
#define yylex  cool_yylex

/* Max size of string constants */
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g++ happy */

extern FILE *fin; /* we read from this file */

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the Cool compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;
int string_chars = 0;

extern int curr_lineno;
extern int verbose_flag;

extern YYSTYPE cool_yylval;

/*
 *  Add Your own definitions here
 */
int nesting = 0;

%}

/*
 * Define names for regular expressions here.
 */
START_COMM      \(\*
END_COMM        \*\)
START_STR       \"
CLASSK          [c|C][l|L][a|A][s|S][s|S]
ELSEK           [e|E][l|L][s|S][e|E]
FIK             [f|F][i|I]
IFK             [i|I][f|F]
INK             [i|I][n|N]
INHERITSK       [i|I][n|N][h|H][e|E][r|R][i|I][t|T][s|S]
LETK            [l|L][e|E][t|T]
LOOPK           [l|L][o|O][o|O][p|P]
POOLK           [p|P][o|O][o|O][l|L]
THENK           [t|T][h|H][e|E][n|N]
WHILEK          [w|W][h|H][i|I][l|L][e|E]
CASEK           [c|C][a|A][s|S][e|E]
ESACK           [e|E][S|s][a|A][c|C]
OFK             [o|O][f|F]
NEWK            [n|N][e|E][w|W]
ISVOIDK         [i|I][s|S][v|V][o|O][i|I][d|D]
NOTK            [n|N][o|O][t|T]

KEYWORD         CLASSK|ELSEK|FIK|IFK|INK|INHERITSK|LETK|LOOPK|POOLK|THENK|WHILEK|CASEK|ESACK|OFK|NEWK|ISVOIDK|NOTK

TRUE            t[r|R][u|U][e|E]
FALSE           f[a|A][l|L][s|S][e|E]

DARROW          =>
LESS_EQUAL      <=

ASSIGNT         <-
DIGIT           [0-9]
ALPHA           [a-zA-Z]
ALPHA_LOWER     [a-z]
ALPHA_UPPER     [A-Z]
        
%%

 /*
  *  Nested comments
  */

<INITIAL>{ 
"(*"  { BEGIN(COMMENT0);  nesting = 1; }
}
<COMMENT0>{

.           ;
"(*"        nesting++;
"*)"        if(--nesting == 0) { BEGIN(INITIAL); }
\n          curr_lineno++;

<<EOF>>     { 
cool_yylval.error_msg = "EOF in comment"; 
BEGIN(INITIAL); 
return ERROR; }

}

<INITIAL>{
"--"   BEGIN(COMMENT1);
}

<COMMENT1>{

.*        ;
\n        { BEGIN(INITIAL); curr_lineno++; }
<<EOF>>   { BEGIN(INITIAL);}

}


 /*
  *  The multiple-character operators.
  */


{DARROW}		{ return (DARROW); }
"+"      {return ('+'); }
"-"      {return ('-'); }
"*"      {return ('*'); }
"/"      {return ('/'); }
";"      {return (';'); }
"."      {return ('.'); }
"@"      {return ('@'); }
"~"      {return ('~'); }
"<"      {return ('<'); }
"="      {return ('='); }
"{"      {return ('{'); }
"}"      {return ('}'); }
"<="     {return (LE); }
","      {return (','); }
":"      {return (':'); }
"("      {return ('('); }
")"      {return (')'); }
{ASSIGNT}  {return ASSIGN; }
[ \t\r\f\v]+ {}

 /*
  * Keywords are case-insensitive except for the values true and false,
  * which must begin with a lower-case letter.
  */

{CLASSK}         {return CLASS; }
{ELSEK}           {return ELSE; }
{FIK}             {return FI; }
{IFK}             {return IF; }
{INK}             {return IN; }
{INHERITSK}       {return INHERITS; }
{LETK}            {return LET; }
{LOOPK}           {return LOOP; }
{POOLK}           {return POOL; }
{THENK}           {return THEN; }
{WHILEK}          {return WHILE; }
{CASEK}           {return CASE; }
{ESACK}           {return ESAC; }
{OFK}             {return OF; }
{NEWK}            {return NEW; }
{ISVOIDK}         {return ISVOID; }
{NOTK}            {return NOT; }


 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */



<INITIAL>{\"  {
 string_buf_ptr = string_buf; 
string_chars = 0;  
BEGIN(STRING); }
}

<STRING>{

\"       { 
*string_buf_ptr++ = '\0';
cool_yylval.symbol = stringtable.add_string(string_buf);
BEGIN(INITIAL);
return STR_CONST;
 }

"\0"   {
if(string_chars != 1024){
*string_buf_ptr++ = '\0'; string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}

\n   {
cool_yylval.error_msg = "Unterminated string constant";
curr_lineno++;
BEGIN(INITIAL);
return ERROR;  }

\0/[^\"] {
cool_yylval.error_msg = "String contains null character";
BEGIN(ERR_STRING);
return ERROR;
}


\\n  {
if(string_chars != 1024){
*string_buf_ptr++ = '\n'; string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}


\\t  {
if(string_chars != 1024){
*string_buf_ptr++ = '\t'; string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}


\\b  {
if(string_chars != 1024){
*string_buf_ptr++ = '\b'; string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}


\\f  {
if(string_chars != 1024){
*string_buf_ptr++ = '\f'; string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}

\\(.|\n)  {
if(string_chars != 1024){
if(yytext[1] == '\0') {
cool_yylval.error_msg = "String contains null character";
BEGIN(ERR_STRING);
return ERROR;
}
*string_buf_ptr++ = yytext[1]; string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}

[^\\\n\"\0]+   {
char *yptr = yytext;
while(*yptr){ 
if(string_chars != 1024){
*string_buf_ptr++ = *yptr++; 
 string_chars++;}

else {
cool_yylval.error_msg = "String constant too long";
BEGIN(ERR_STRING);
return ERROR;
}}}

<<EOF>> {
cool_yylval.error_msg = "EOF in string constant";
BEGIN(INITIAL);
return ERROR; }

}






<ERR_STRING>{

[^\n\"]*\n    curr_lineno++; BEGIN(INITIAL);
[^\n\"]*\"    BEGIN(INITIAL);

}     










[A-Z][a-zA-Z0-9_]* {
cool_yylval.symbol = idtable.add_string(yytext);
return TYPEID; 
}

{FALSE} {
cool_yylval.boolean = false;
return BOOL_CONST;
}

{TRUE}     {
cool_yylval.boolean = true;
return BOOL_CONST;
}

[0-9]+ {
cool_yylval.symbol = inttable.add_string(yytext);
return INT_CONST;
}

[a-z][a-zA-Z0-9_]*   {
cool_yylval.symbol = idtable.add_string(yytext);
return OBJECTID;
}

"*)"     {cool_yylval.error_msg = "Unmatched *)"; return ERROR; }

\n      curr_lineno++;

.      {
cool_yylval.error_msg = yytext;
return ERROR; }

%%
